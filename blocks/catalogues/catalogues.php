<?php
/**
 * The template for the catalogues block
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

// Defining usage.
use function gital_library\sanitize_file_name;
use function gital_library\get_field_group_values;

if ( is_admin() ) {
	?>
	<div style="padding:16px 20px;border:#adb2ad solid 1px;">
		<p style="text-align:center;font-size:2rem;"><?php echo esc_html__( 'Catalogues', 'gital-cat' ); ?></p>
		<p style="text-align:center;"><?php echo esc_html__( 'This is a placeholder for the backend.', 'gital-cat' ); ?></p>
	</div>
	<?php
	return;
}

$data = json_encode( get_field_group_values( 'g_cat_tag_fields' ) );
?>
<div class="g-cat">
	<div id="g-cat-selection" class="g-cat__selection g-cat-selection" catalogue-selection="<?php echo esc_attr( $data ); ?>">
		<?php
		$count_catalogues = wp_count_posts( 'g_cat_catalogue' );
		$count_catalogues->publish;

		for ( $i = 0; $i < $count_catalogues->publish; $i++ ) {
			echo '<div class="g-cat-thumbnail g-cat-thumbnail--placeholder"><div class="g-cat-thumbnail__image"></div><div class="g-cat-thumbnail__title"></div></div>';
		}
		?>
	</div>
</div>