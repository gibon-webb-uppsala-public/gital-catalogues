import * as flipbook from "./vendor/index.js";
import * as pdf_js from "pdfjs-dist/build/pdf.mjs";

/**
 * Initializes the PDF.js library by setting a global worker with the necessary path.
 * This worker is essential for the library to process and render PDFs in the browser.
 * If the `GlobalWorkerOptions` is not available on `pdf_js`, an error will be thrown.
 * 
 * @throws {Error} Throws an error if `pdf_js.GlobalWorkerOptions` is undefined.
 */
const pdf_js_init = () => {
	if (pdf_js.GlobalWorkerOptions) {
		pdf_js.GlobalWorkerOptions.workerPort = new Worker(window.g_cat_paths.scripts_path + '/gital.catalogues.worker.min.js');
	} else {
		throw new Error("pdf_js.GlobalWorkerOptions is undefined.");
	}
}

/**
 * Updates the current URL by adding or updating a query parameter for the book's ID.
 * This function takes the provided ID, encodes it for URL usage, and sets it in the
 * 'g-cat-id' query parameter. It then updates the browser's history with the new URL
 * without causing a page reload.
 *
 * @param {string} id - The ID of the book to be set in the query parameter.
 */
const add_book_id_from_url_query = (id) => {
	const location = window.location;
	const base_url = `${location.protocol}//${location.host}${location.pathname}`;
	const search_params = new URLSearchParams(location.search);
	search_params.set('g-cat-id', encodeURIComponent(id));
	const new_url = `${base_url}?${search_params.toString()}`;
	window.history.pushState({ path: new_url }, '', new_url);
}

/**
 * Removes the 'g-cat-id' query parameter from the current URL. After deletion,
 * it updates the browser's history with the new URL without causing a page reload.
 */
const remove_book_id_from_url_query = () => {
	const location = window.location;
	const base_url = `${location.protocol}//${location.host}${location.pathname}`;
	const search_params = new URLSearchParams(location.search);
	search_params.delete('g-cat-id');
	const new_url = `${base_url}?${search_params.toString()}`;
	window.history.pushState({}, '', new_url);
}

/**
 * Initializes a PDF viewing process, caches pages for faster access, and provides a method to terminate the viewing.
 *
 * @param {string} pdf_link - The link to the PDF to be opened.
 * @param {Function} cb - The callback function to handle the initial setup result or errors.
 * @param {string|number} id - The unique identifier for the book to add to the URL query parameters.
 * @returns {Function} A function to terminate the PDF viewing process and clear the cache.
 */
const book = (pdf_link, cb, id) => {
	pdf_js_init();
	let is_terminated = false;
	const cache = [];
	add_book_id_from_url_query(id);

	const terminate_and_clear_cache = () => {
		remove_book_id_from_url_query();
		is_terminated = true;
		cache.pop();
	};

	const get_page = (pdf, page_number, cb) => {
		if (is_terminated) {
			return;
		}
		if (!page_number || page_number > pdf.numPages) {
			return cb();
		}
		if (cache[page_number]) {
			return cb(null, cache[page_number]);
		}

		pdf.getPage(page_number)
			.then(page => {
				const scale = 1.2;
				const viewport = page.getViewport({ scale })
				const output_scale = window.devicePixelRatio || 1;
				const canvas = document.createElement('canvas');

				canvas.width = Math.floor(viewport.width * output_scale);
				canvas.height = Math.floor(viewport.height * output_scale);
				canvas.style.width = Math.floor(viewport.width) + "px";
				canvas.style.height = Math.floor(viewport.height) + "px";

				const transform = output_scale !== 1 ? [output_scale, 0, 0, output_scale, 0, 0] : null;
				const context = canvas.getContext("2d")
				const render_context = {
					canvasContext: context,
					transform,
					viewport,
				}
				page.render(render_context).promise
					.then(() => {
						const img = new Image();
						img.src = canvas.toDataURL();
						img.addEventListener("load", () => {
							cache[page_number] = {
								img,
								num: page_number,
								width: img.width,
								height: img.height,
							}
							cb(null, cache[page_number]);
						}, false);
					})
					.catch(err => cb(err));
			})
			.catch(err => cb(err));
	}

	pdf_js.getDocument(pdf_link).promise
		.then(pdf => {
			cb(null, {
				pdf,
				numPages: () => pdf.numPages,
				getPage: (page_number, cb) => get_page(pdf, page_number, cb)
			})
		})
		.catch(err => cb(err || "pdf parsing failed"));

	return terminate_and_clear_cache;
}

/**
 * Fetches catalogue IDs from a specific endpoint asynchronously.
 *
 * @async
 * @returns {Promise<Object>} A promise that resolves to the JSON response containing the catalogue IDs.
 */
const fetch_catalogue_ids = async (selection) => {
	const response = await fetch('/wp-json/gital/catalogues/fetch_catalogue_ids', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			selection,
		}),
	});
	return response.json();
};

/**
 * Fetches catalogue details based on the provided ids.
 *
 * @async
 * @param {Array<number|string>} ids - An array of catalogue IDs to fetch information for.
 * @returns {Promise<Object>} A promise that resolves to the JSON response containing catalogue details.
 */
const fetch_catalogues = async (ids) => {
	const response = await fetch('/wp-json/gital/catalogues/fetch_catalogues', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			ids,
		}),
	});
	return response.json();
};

/**
 * Loads a book in the form of a PDF and initializes flipbook viewer.
 *
 * @param {string} pdf - The URL or path to the PDF file to be loaded into flipbook.
 * @param {number|string} id - The ID associated with the book to be used for reference or retrieval.
 * @returns {Function} A function that can be called to terminate the book view instance and clean up resources.
 */
const load_book = (pdf, id) => {
	const anchor = document.getElementById('g-cat-catalogue');
	const width = window.innerWidth - 48;
	const single_page = window.innerWidth < 500 ? true : false;
	const options = {
		width: width,
		height: window.innerHeight - 48,
		backgroundColor: "#fff",
		boxColor: "#fff",
		singlepage: single_page
	}
	const buttons = {
		'next': document.getElementById('g-cat-catalogue-next'),
		'prev': document.getElementById('g-cat-catalogue-prev'),
		'zoom': document.getElementById('g-cat-catalogue-zoom'),
	}
	let terminate_book;

	anchor.classList.add('g-cat-catalogue--loading');
	window.g_show_modal_from_id('g-cat-catalogue-modal');
	document.body.classList.add('g-body--g-cat-open');
	anchor.style.setProperty('--page-width', anchor.clientWidth + 'px');
	
	terminate_book = book(pdf, (err, catalogue) => {
		if (err) {
			console.error(err);
		} else {
			flipbook.init(catalogue, anchor, options, (err, viewer) => {
				if (err) {
					return console.error(err);
				}
				
				if (!single_page) {
					viewer.on('seen', () => {
						anchor.classList.remove('g-cat-catalogue--loading');
					})
					buttons.next.onclick = () => viewer.flip_forward();
					buttons.prev.onclick = () => viewer.flip_back();
					buttons.zoom.onclick = () => viewer.zoom();
				}
			});
		}

	}, id);

	return terminate_book;
}

/**
 * Sets up a handler for loading and reloading a book via the `load_book` function 
 * in response to window resizing events, ensuring that the book is properly displayed 
 * on various screen sizes.
 *
 * @param {string} pdf - The URL or path to the PDF file to load into the flipbook.
 * @param {number|string} id - A unique identifier for the specific book instance.
 */
const book_loader_handler = (pdf, id) => {
	const anchor = document.getElementById('g-cat-catalogue');
	const terminate_book = load_book(pdf, id);
	const modal = document.getElementById('g-cat-catalogue-modal');
	let debounce_resize_timer;
	let width = window.innerWidth;
	let single_page = width < 500;

	const reload_book = () => {
		clearTimeout(debounce_resize_timer);
		debounce_resize_timer = setTimeout(() => {
			const new_width = window.innerWidth;
			const changed_single_page_state = new_width < 500 !== single_page;
			const has_new_width = width !== new_width;
			width = new_width;

			if (changed_single_page_state) {
				single_page = new_width < 500 ? true : false;
			}

			if (changed_single_page_state || has_new_width && !single_page) {
				window.removeEventListener('resize', reload_book);
				terminate_book();
				book_loader_handler(pdf, id);
			}

			if (single_page) {
				const anchor = document.getElementById('g-cat-catalogue');				
				anchor.style.setProperty('--page-width', anchor.clientWidth + 'px');
			}
		}, 500);
	};

	window.addEventListener('resize', reload_book);

	const reset_modal_and_terminate_book = () => {
		setTimeout(() => {
			anchor.classList.add('g-cat-catalogue--loading');
		}, 500);
		document.body.classList.remove('g-body--g-cat-open');
		terminate_book();
		anchor.innerHTML = '';
		modal.removeEventListener('closing', reset_modal_and_terminate_book);
		window.removeEventListener('resize', reload_book);
		if (debounce_resize_timer) {
			clearTimeout(debounce_resize_timer);
		}
	}

	modal.addEventListener('closing', reset_modal_and_terminate_book);
}

/**
 * Renders HTML content for catalogues into a specified DOM element and sets up click listeners 
 * for each catalogue thumbnail, which will trigger the function `book_loader_handler`.
 * If a catalogue id matches the query parameter 'g-cat-id' in the current URL,
 * it automatically triggers its associated handler.
 *
 * @param {string} html - A string of HTML to be rendered inside the anchor element.
 * @param {HTMLElement} anchor - The DOM element where the catalogue HTML should be injected.
 */
const render_catalogues = (html, anchor) => {
	if (!anchor) {
		return;
	}
	const query_id = new URL(window.location.href).searchParams.get('g-cat-id');
	anchor.innerHTML = html;
	const catalogues = anchor.querySelectorAll('.g-cat-thumbnail');
	if (catalogues) {
		catalogues.forEach(catalogue => {
			const pdf = catalogue.getAttribute('catalogue-url');
			const id = catalogue.getAttribute('catalogue-id');

			catalogue.addEventListener('click', () => {
				book_loader_handler(pdf, id);
			});

			if (query_id && id === query_id) {
				book_loader_handler(pdf, id);
			}
		});
	}
}

/**
 * Initializes the catalogue selection process.
 * It fetches the catalogue IDs and, if successful,
 * proceeds to fetch the complete catalogues data.
 * Once the catalogue data is fetched, it calls `render_catalogues`
 * to inject the HTML into the designated anchor element on the page.
 */
const init = () => {
	const selection_anchor = document.getElementById('g-cat-selection');
	const selection = selection_anchor.getAttribute('catalogue-selection');

	if (selection_anchor) {
		fetch_catalogue_ids(selection)
			.then((response) => {
				if (response && response.length !== 0) {
					fetch_catalogues(response).then((response) => {
						render_catalogues(response, selection_anchor);
					})
						.catch((error) => {
							console.error('Error:', error);
						});
				}
			})
			.catch((error) => {
				console.error('Error:', error);
			});
	}
}

window.g_cat_init = init;
