��          �       �      �     �     �     �  	   �  
          4   !     V  <   q     �     �     �     �     �  b   �  U   B  	   �  	   �     �     �  	   �     �     �        &        <     B  �  F     �     �     �       	          3   -  #   a  ;   �     �     �     �     �     �  o     X   r  
   �     �     �     �     �               #  (   C     l     s   All All %s Alternative cover Catalogue Catalogues Customized Blocks Higher value lists the catalogue higher in the list. Holds our %s specific data If no image is given, the first page of the catalog is used. Manual No No %s found No %s found in Trash Order Please enable <strong>Advanced Custom Fields PRO</strong> in order to use Gital Visual Statistics. Please enable <strong>Gital Library</strong> in order to use Gital Visual Statistics. Published Search %s Select catalogues Select tags Selection Selection of catalogues Show the catalogues Swipe left to browse This is a placeholder for the backend. Title Yes Project-Id-Version: Gital Catalogues
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2024-04-10 12:19+0000
PO-Revision-Date: 2024-04-15 08:48+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.7; wp-6.5.2
X-Domain: gital-cat Alla Alla %s Alternativt omslag Katalog Kataloger Anpassade block Högre värde listar katalogen högre upp i listan. Innehåller våra %s specifika data Om ingen bild anges används den första sidan i katalogen. Manuellt Nej Inga %s funna Inga %s funna i papperskorgen Ordning Vänligen aktivera <strong>Advanced Custom Fields PRO</strong> för att kunna använda Gital Visual Statistics. Aktivera <strong>Gital Library</strong> för att kunna använda Gital Visual Statistics. Publicerad Sök %s Välj kataloger Välj taggar Urval Urval av kataloger Visa kataloger Svep vänster för att bläddra Detta är en platshållare för backend. Rubrik Ja 