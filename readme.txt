=== Gital Catalogues ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 6.4
Requires PHP: 7.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Catalogues is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.

== Description ==
The Gital Catalogues is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.

== Changelog ==

= 1.5.7 - 2024.04.15 = 
* Update: The title in the thumb is now split at "-".

= 1.4.0 - 2024.04.15 = 
* Update: Added notification if the viewer does not swipe in 1.5s.
* Update: Improved the cache handling.

= 1.3.5 - 2024.04.10 = 
* Update: Added the sorting of the Order column in the backend.

= 1.2.0 - 2024.04.10 = 
* Update: Added checkbox to the post type column.

= 1.1.1 - 2024.04.10 = 
* Update: Release candidate.

= 1.0.0 - 2024.04.10 = 
* Update: Init.