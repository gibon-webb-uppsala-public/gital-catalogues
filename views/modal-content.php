<?php
/**
 * Modal content
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

use function gital_library\icon;
?>

<div id="g-cat-catalogue" class="g-cat-catalogue g-cat-catalogue--loading"></div>
<?php
echo icon(
	G_CAT_ICONS . '/arrow_left.svg',
	true,
	array(
		'id'           => 'g-cat-catalogue-prev',
		'class'        => 'g-cat-button g-cat-button--prev',
		'width'        => 30,
		'height'       => 60,
		'style'        => array(
			'opacity' => 0,
		),
		'data-no-lazy' => '1',
	),
	'svg',
	true
);
echo icon(
	G_CAT_ICONS . '/arrow_right.svg',
	true,
	array(
		'id'           => 'g-cat-catalogue-next',
		'class'        => 'g-cat-button g-cat-button--next',
		'width'        => 30,
		'height'       => 60,
		'style'        => array(
			'opacity' => 0,
		),
		'data-no-lazy' => '1',
	),
	'svg',
	true
);
echo icon(
	G_CAT_ICONS . '/magnifier.svg',
	true,
	array(
		'id'           => 'g-cat-catalogue-zoom',
		'class'        => 'g-cat-button g-cat-button--zoom',
		'width'        => 30,
		'height'       => 60,
		'style'        => array(
			'opacity' => 0,
		),
		'data-no-lazy' => '1',
	),
	'svg',
	true
);