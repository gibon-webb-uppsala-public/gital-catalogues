<?php
/**
 * Plugin Name: Gital Catalogues
 * Author: Gibon Webb Uppsala
 * Version: 1.5.7
 *
 * Text Domain: gital-cat
 * Domain Path: /languages/
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Catalogues is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb.uppsala@gibon.se.
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

use gital_library\Modal;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-cat', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_cat\textdomain' );

/**
 * Check dependencies
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function check_dependencies() {
	require_once ABSPATH . 'wp-admin/includes/plugin.php';
	if ( ! function_exists( 'deactivate_plugins' ) ) {
		return;
	}

	if ( ! is_plugin_active( 'gital-library/gital-library.php' ) ) {
		deactivate_plugins( 'gital-catalogues/gital-catalogues.php' );
		add_action(
			'admin_notices',
			function () {
				?>
				<div class="notice error">
					<p>
						<?php wp_kses( _e( 'Please enable <strong>Gital Library</strong> in order to use Gital Catalogues.', 'gital-cat' ), array( 'strong' => array() ) ); ?>
					</p>
				</div>
				<?php
			}
		);
	}

	if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
		deactivate_plugins( 'gital-catalogues/gital-catalogues.php' );
		add_action(
			'admin_notices',
			function () {
				?>
				<div class="notice error">
					<p>
						<?php wp_kses( _e( 'Please enable <strong>Advanced Custom Fields PRO</strong> in order to use Gital Catalogues.', 'gital-cat' ), array( 'strong' => array() ) ); ?>
					</p>
				</div>
				<?php
			}
		);
	}
}
add_action( 'plugins_loaded', 'gital_cat\check_dependencies' );

// Constants.
if ( ! defined( 'G_CAT_ROOT' ) ) {
	define( 'G_CAT_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_CAT_ASSETS' ) ) {
	define( 'G_CAT_ASSETS', G_CAT_ROOT . '/assets' );
}
if ( ! defined( 'G_CAT_SCRIPTS' ) ) {
	define( 'G_CAT_SCRIPTS', G_CAT_ASSETS . '/scripts' );
}
if ( ! defined( 'G_CAT_ICONS' ) ) {
	define( 'G_CAT_ICONS', G_CAT_ASSETS . '/icons' );
}
if ( ! defined( 'G_CAT_RESOURSES' ) ) {
	define( 'G_CAT_RESOURSES', G_CAT_ROOT . '/assets/resources' );
}
if ( ! defined( 'G_CAT_ROOT_PATH' ) ) {
	define( 'G_CAT_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_CAT_ASSETS_PATH' ) ) {
	define( 'G_CAT_ASSETS_PATH', G_CAT_ROOT_PATH . 'assets/' );
}
if ( ! defined( 'G_CAT_FUNCTIONS_PATH' ) ) {
	define( 'G_CAT_FUNCTIONS_PATH', G_CAT_ROOT_PATH . 'functions/' );
}
if ( ! defined( 'G_CAT_CLASSES_PATH' ) ) {
	define( 'G_CAT_CLASSES_PATH', G_CAT_ROOT_PATH . 'classes/' );
}
if ( ! defined( 'G_CAT_VIEWS_PATH' ) ) {
	define( 'G_CAT_VIEWS_PATH', G_CAT_ROOT_PATH . 'views/' );
}
if ( ! defined( 'G_CAT_VENDOR_PATH' ) ) {
	define( 'G_CAT_VENDOR_PATH', G_CAT_ROOT_PATH . 'vendor/' );
}
if ( ! defined( 'G_CAT_BLOCKS_PATH' ) ) {
	define( 'G_CAT_BLOCKS_PATH', G_CAT_ROOT_PATH . 'blocks/' );
}

// Autoloader.
require G_CAT_VENDOR_PATH . 'autoload.php';

// Init the updater.
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-catalogues.json',
	__FILE__,
	'gital-catalogues'
);

/**
 * Enquene public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function assets() {
	if ( has_block( 'acf/g-cat-catalogues' ) ) {
		wp_register_style( 'g_cat_style', G_CAT_ASSETS . '/styles/gital.catalogues.min.css', array(), '1.0.0' );
		wp_enqueue_style( 'g_cat_style' );

		wp_register_script( 'g_cat_script', G_CAT_ASSETS . '/scripts/gital.catalogues.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'g_cat_script' );

		wp_add_inline_script( 'g_cat_script', 'window.addEventListener("load", g_cat_init);', 'after' );
		wp_localize_script( 'g_cat_script', 'g_cat_paths', array( 'scripts_path' => G_CAT_SCRIPTS ) );
		wp_localize_script( 'g_cat_script', 'g_cat_trans', array( 'swipe_hint' => __( 'Swipe left to browse', 'gital-cat' ) ) );
		ob_start();
		require G_CAT_VIEWS_PATH . 'modal-content.php';
		$modal_content = ob_get_clean();
		$modal         = new Modal( $modal_content, 'g-cat-catalogue-modal', 'g-cat-catalogue-modal', '', true, true, false );
		$modal->the_modal_to_footer();
	}
}
add_action( 'wp_enqueue_scripts', 'gital_cat\assets' );

// Classes.
Post_Type::get_instance();
Block_Registration::get_instance();
Rest_Routes::get_instance();