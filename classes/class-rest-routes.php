<?php
/**
 * Rest Routes
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

if ( ! class_exists( 'Rest_Routes' ) ) {
	/**
	 * Rest Routes
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.0
	 */
	class Rest_Routes extends Singleton {
		/**
		 * Init
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			add_action( 'rest_api_init', array( $this, 'rest_routes' ) );
		}

		/**
		 * Register a new route for the REST API
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function rest_routes() {
			register_rest_route(
				'gital',
				'/catalogues/fetch_catalogue_ids',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'fetch_catalogue_ids_callback' ),
					'permission_callback' => '__return_true',
				)
			);
			register_rest_route(
				'gital',
				'/catalogues/fetch_catalogues',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'fetch_catalogues_callback' ),
					'permission_callback' => '__return_true',
				)
			);
		}

		public function fetch_catalogue_ids_callback( $payload = false ) {
			$parameters = ! empty( $payload ) ? $payload->get_params() : false;

			$defaults = array(
				'selection' => 'all',
				'manual'    => array(),
				'tags'      => array(),
			);

			$selection = wp_parse_args( json_decode( $parameters['selection'] ?? '' ), $defaults );

			$args = array(
				'fields'         => 'ids',
				'post_type'      => 'g_cat_catalogue',
				'meta_key'       => 'order',
				'orderby'        => 'meta_value_num',
				'order'          => 'DESC',
				'posts_per_page' => -1,
			);

			if ( 'manual' === $selection['selection'] && ! empty( $selection['manual'] ) ) {
				$args += array(
					'post__in' => $selection['manual'],
				);
			}

			if ( 'tags' === $selection['selection'] && ! empty( $selection['tags'] ) ) {
				$args += array(
					'tax_query' => array(
						array(
							'taxonomy' => 'g_cat_tag',
							'field'    => 'term_id',
							'terms'    => $selection['tags'],
						),
					),
				);
			}

			return get_posts( $args );
		}

		public function fetch_catalogues_callback( $payload = false ) {
			$parameters = ! empty( $payload ) ? $payload->get_params() : false;

			$defaults = array(
				'ids' => array(),
			);

			$parameters = wp_parse_args( $parameters, $defaults );

			if ( empty( $parameters['ids'] ) ) {
				return;
			}

			$html_string = '';

			foreach ( $parameters['ids'] as $id ) {
				$html_string .= Components::thumbnail( $id );
			}

			return $html_string;
		}
	}
}
