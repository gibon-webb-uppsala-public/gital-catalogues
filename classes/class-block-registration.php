<?php
/**
 * Block Registration
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

if ( ! class_exists( 'Block_Registration' ) ) {
	/**
	 * Block Registration
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.0
	 */
	class Block_Registration extends Singleton {
		/**
		 * Init
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			add_filter( 'block_categories_all', array( $this, 'block_categories' ), 10, 1 );
			add_action( 'init', array( $this, 'block_visual_statistics' ) );
		}

		/**
		 * Add custom block category
		 *
		 * @param array $categories The block categories.
		 *
		 * @return array $categories The custom block categories.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function block_categories( $categories ) {
			$slugs = array_column( $categories, 'slug' );
			if ( in_array( 'customized-blocks', $slugs, true ) ) {
				return $categories;
			}

			$categories[] = array(
				'slug'  => 'customized-blocks',
				'title' => __( 'Customized Blocks', 'gital-cat' ),
			);
			return $categories;
		}

		/**
		 * Register the Visual Statistics block
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function block_visual_statistics() {
			register_block_type_from_metadata(
				G_CAT_BLOCKS_PATH . 'catalogues',
				array(
					'title'       => __( 'Catalogues', 'gital-cat' ),
					'description' => __( 'Show the catalogues', 'gital-cat' ),
				)
			);
		}
	}
}
