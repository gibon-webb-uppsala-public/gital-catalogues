<?php
/**
 * Post Type
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

if ( ! class_exists( 'Post_Type' ) ) {
	/**
	 * Post Type
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.2.0
	 */
	class Post_Type extends Singleton {
		/**
		 * Construct
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			add_action( 'init', array( $this, 'create_catalogue_post_type' ) );
			add_action( 'init', array( $this, 'create_catalogue_tag_taxonomy' ) );
			add_filter( 'manage_g_cat_catalogue_posts_columns', array( $this, 'manage_catalogue_columns' ) );
			add_action( 'manage_g_cat_catalogue_posts_custom_column', array( $this, 'manage_catalogue_columns_content' ), 10, 2 );
			add_action( 'acf/include_fields', array( $this, 'register_fields' ) );
			add_action( 'pre_get_posts', array( $this, 'manage_catalogue_columns_sort_order' ) );
			add_filter( 'manage_edit-g_cat_catalogue_sortable_columns', array( $this, 'manage_catalogue_columns_sort_order_key' ) );
		}

		/**
		 * Registers the Catalogues post type
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function create_catalogue_post_type() {
			$singular                      = 'Catalogue';
			$plural                        = 'Catalogues';
			$singular_translated           = __( 'Catalogue', 'gital-cat' );
			$plural_translated             = __( 'Catalogues', 'gital-cat' );
			$singular_lowercase            = strtolower( $singular );
			$plural_lowercase              = strtolower( $plural );
			$singular_lowercase_translated = strtolower( $singular_translated );
			$plural_lowercase_translated   = strtolower( $plural_translated );
			$slug                          = 'g_cat_catalogue';

			register_post_type(
				$slug,
				array(
					'labels'       => array(
						'name'               => $plural_translated,
						'singular_name'      => $singular_translated,
						'menu_name'          => $plural_translated,
						'name_admin_bar'     => $singular_translated,
						'all_items'          => sprintf( __( 'All %s', 'gital-cat' ), $plural_translated ),
						'search_items'       => sprintf( __( 'Search %s', 'gital-cat' ), $plural_translated ),
						'not_found'          => sprintf( __( 'No %s found', 'gital-cat' ), $plural_translated ),
						'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'gital-cat' ), $plural_translated ),
					),
					'description'  => sprintf( __( 'Holds our %s specific data', 'gital-cat' ), $singular_lowercase ),
					'public'       => false,
					'show_ui'      => true,
					'show_in_menu' => true,
					'has_archive'  => false,
					'menu_icon'    => 'dashicons-book',
					'supports'     => array( 'title' ),
					'hierarchical' => false,
					'taxonomies'   => array( 'g_cat_tag' ),
				)
			);
		}

		/**
		 * Registers the Catalogue tag taxonomy
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function create_catalogue_tag_taxonomy() {
			$singular                      = 'Tag';
			$plural                        = 'Tags';
			$singular_translated           = __( 'Tag', 'gital-blocks' );
			$plural_translated             = __( 'Tags', 'gital-blocks' );
			$singular_lowercase            = strtolower( $singular );
			$plural_lowercase              = strtolower( $plural );
			$singular_lowercase_translated = strtolower( $singular_translated );
			$plural_lowercase_translated   = strtolower( $plural_translated );
			$slug                          = 'g_cat_tag';

			register_taxonomy(
				$slug,
				'g_cat_catalogue',
				array(
					'labels'       => array(
						'name'              => $plural_translated,
						'singular_name'     => $singular_translated,
						'menu_name'         => $plural_translated,
						'add_new_item'      => sprintf( __( 'Add New %s', 'gital-blocks' ), $singular_translated ),
						'new_item'          => sprintf( __( 'New %s', 'gital-blocks' ), $singular_translated ),
						'edit_item'         => sprintf( __( 'Edit %s', 'gital-blocks' ), $singular_translated ),
						'update_item'       => sprintf( __( 'Update %s', 'gital-blocks' ), $singular_translated ),
						'view_item'         => sprintf( __( 'View %s', 'gital-blocks' ), $singular_translated ),
						'new_item_name'     => sprintf( __( 'New %s Name', 'gital-blocks' ), $singular_translated ),
						'all_items'         => sprintf( __( 'All %s', 'gital-blocks' ), $plural_translated ),
						'search_items'      => sprintf( __( 'Search %s', 'gital-blocks' ), $plural_translated ),
						'back_to_items'     => sprintf( __( 'Back to %s', 'gital-blocks' ), $plural_lowercase_translated ),
						'parent_item'       => sprintf( __( 'Parent %s', 'gital-blocks' ), $plural_lowercase_translated ),
						'parent_item_colon' => sprintf( __( 'Parent %s:', 'gital-blocks' ), $plural_lowercase_translated ),
						'not_found'         => sprintf( __( 'No %s found', 'gital-blocks' ), $plural_translated ),
					),
					'public'       => false,
					'rewrite'      => false,
					'show_ui'      => true,
					'hierarchical' => false,
				)
			);
		}

		/**
		 * Manages the Catalogues admin columns
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function manage_catalogue_columns( $columns ) {
			$columns = array(
				'cb'        => $columns['cb'],
				'title'     => __( 'Title', 'gital-cat' ),
				'order'     => __( 'Order', 'gital-cat' ),
				'published' => __( 'Published', 'gital-cat' ),
			);

			return $columns;
		}

		/**
		 * Manages the Catalogues admin columns content
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 */
		public function manage_catalogue_columns_content( $column, $post_id ) {
			switch ( $column ) {
				case 'order':
					echo esc_html( get_field( 'order' ) );
					break;

				case 'published':
					echo esc_html( 'publish' === get_post_status( $post_id ) ? __( 'Yes', 'gital-cat' ) : __( 'No', 'gital-cat' ) );
					break;

				default:
					break;
			}
		}

		/**
		 * Register fields
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function register_fields() {
			if ( ! function_exists( 'acf_add_local_field_group' ) ) {
				return;
			}

			acf_add_local_field_group(
				array(
					'key'                   => 'g_cat_catalogue_fields',
					'title'                 => __( 'Catalogue', 'gital-cat' ),
					'fields'                => array(
						array(
							'key'               => 'g_cat_catalogue_field_alt_thumbnail',
							'label'             => __( 'Alternative cover', 'gital-cat' ),
							'name'              => 'thumbnail',
							'aria-label'        => '',
							'type'              => 'image',
							'instructions'      => __( 'If no image is given, the first page of the catalog is used.', 'gital-cat' ),
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '45',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'id',
							'library'           => 'all',
							'min_width'         => '',
							'min_height'        => '',
							'min_size'          => '',
							'max_width'         => '',
							'max_height'        => '',
							'max_size'          => '',
							'mime_types'        => '',
							'preview_size'      => 'thumbnail',
						),
						array(
							'key'               => 'g_cat_catalogue_field_file',
							'label'             => __( 'Catalogue', 'gital-cat' ),
							'name'              => 'catalogue',
							'aria-label'        => '',
							'type'              => 'file',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '45',
								'class' => '',
								'id'    => '',
							),
							'return_format'     => 'id',
							'library'           => 'all',
							'min_size'          => '',
							'max_size'          => '',
							'mime_types'        => 'pdf',
						),
						array(
							'key'               => 'g_cat_catalogue_field_order',
							'label'             => __( 'Order', 'gital-cat' ),
							'name'              => 'order',
							'aria-label'        => '',
							'type'              => 'number',
							'instructions'      => __( 'Higher value lists the catalogue higher in the list.', 'gital-cat' ),
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '10',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'min'               => 0,
							'max'               => '',
							'placeholder'       => '',
							'step'              => 1,
							'prepend'           => '',
							'append'            => '',
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post_type',
								'operator' => '==',
								'value'    => 'g_cat_catalogue',
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'seamless',
					'label_placement'       => 'top',
					'instruction_placement' => 'field',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
					'show_in_rest'          => 0,
				)
			);

			acf_add_local_field_group(
				array(
					'key'                   => 'g_cat_tag_fields',
					'title'                 => __( 'Selection of catalogues', 'gital-cat' ),
					'fields'                => array(
						array(
							'key'               => 'g_cat_tag_fields_selection',
							'label'             => __( 'Selection', 'gital-cat' ),
							'name'              => 'selection',
							'aria-label'        => '',
							'type'              => 'button_group',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'choices'           => array(
								'all'    => __( 'All', 'gital-cat' ),
								'tags'   => __( 'Select tags', 'gital-cat' ),
								'manual' => __( 'Manual', 'gital-cat' ),
							),
							'default_value'     => 'all',
							'return_format'     => 'value',
							'allow_null'        => 0,
							'layout'            => 'horizontal',
						),
						array(
							'key'                  => 'g_cat_tag_fields_catalogues',
							'label'                => __( 'Select catalogues', 'gital-cat' ),
							'name'                 => 'manual',
							'aria-label'           => '',
							'type'                 => 'post_object',
							'instructions'         => '',
							'required'             => 0,
							'conditional_logic'    => array(
								array(
									array(
										'field'    => 'g_cat_tag_fields_selection',
										'operator' => '==',
										'value'    => 'manual',
									),
								),
							),
							'wrapper'              => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'post_type'            => array(
								0 => 'g_cat_catalogue',
							),
							'post_status'          => array(
								0 => 'publish',
							),
							'taxonomy'             => '',
							'return_format'        => 'id',
							'multiple'             => 1,
							'allow_null'           => 0,
							'bidirectional'        => 0,
							'ui'                   => 1,
							'bidirectional_target' => array(),
						),
						array(
							'key'                  => 'g_cat_tag_fields_tags',
							'label'                => __( 'Select tags', 'gital-cat' ),
							'name'                 => 'tags',
							'aria-label'           => '',
							'type'                 => 'taxonomy',
							'instructions'         => '',
							'required'             => 0,
							'conditional_logic'    => array(
								array(
									array(
										'field'    => 'g_cat_tag_fields_selection',
										'operator' => '==',
										'value'    => 'tags',
									),
								),
							),
							'wrapper'              => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'taxonomy'             => 'g_cat_tag',
							'add_term'             => 1,
							'save_terms'           => 1,
							'load_terms'           => 1,
							'return_format'        => 'id',
							'field_type'           => 'checkbox',
							'bidirectional'        => 0,
							'multiple'             => 0,
							'allow_null'           => 0,
							'bidirectional_target' => array(),
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'block',
								'operator' => '==',
								'value'    => 'acf/g-cat-catalogues',
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
					'show_in_rest'          => 0,
				)
			);
		}

		/**
		 * Manage the catalogue sorting by the order field
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function manage_catalogue_columns_sort_order( $query ) {
			if ( ! is_admin() || ! $query->is_main_query() ) {
				return;
			}

			if ( 'g_cat_catalogue_order' === $query->get( 'orderby' ) ) {
				$query->set( 'orderby', 'meta_value' );
				$query->set( 'meta_key', 'order' );
				$query->set( 'meta_type', 'numeric' );
			}
		}

		/**
		 * Manage the catalogue sorting by the order field key
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function manage_catalogue_columns_sort_order_key( $columns ) {

			$columns['order'] = 'g_cat_catalogue_order';
			return $columns;
		}
	}
}
