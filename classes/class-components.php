<?php
/**
 * Components
 *
 * @package Gital Catalogues
 */

namespace gital_cat;

// Defining usage.
use function gital_library\clean_classes;
use function gital_library\image_src;
use function gital_library\get_field_group_values;

if ( ! class_exists( 'Components' ) ) {
	/**
	 * Components
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.0
	 */
	class Components {
		public static function thumbnail( $id ) {
			$title_id          = 'g-cat-thumbnail-title--' . wp_rand( 1000, 9999 );
			$data              = get_field_group_values( 'g_cat_catalogue_fields', false, $id );
			$data['thumbnail'] = $data['thumbnail'] ? $data['thumbnail'] : $data['catalogue'];

			$component   = '<div class="g-cat-thumbnail" labeledby="' . $title_id . '" catalogue-id="' . $data['catalogue'] . '" catalogue-url="' . esc_attr( wp_get_attachment_url( $data['catalogue'] ) ) . '">';
			$component  .= self::image( $data['thumbnail'], 'g-cat-thumbnail__image' );
			$title       = get_the_title( $id );
			$title       = str_replace( array( '–', '&#8211;' ), '-', $title );
			$title_parts = explode( '-', $title );
			$component  .= '<p class="g-cat-thumbnail__title" id="' . $title_id . '">';
			foreach ( $title_parts as $part ) {
				$component .= '<span>' . trim( $part ) . '</span>';
			}
			$component .= '</p>';
			$component .= '</div>';
			return $component;
		}

		/**
		 * Image
		 *
		 * @param array  $media The media data. See $defaults for details.
		 * @param string $additional_classes Additional classes.
		 * @param string $id The id of the component.
		 *
		 * @return string The image component.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 */
		public static function image( $image_id, $additional_classes = '' ) {
			$dom       = new \DOMDocument();
			$component = $dom->createElement( 'img' );
			$component->setAttribute( 'class', clean_classes( array( 'g-cat-image', $additional_classes ) ) );
			$component->setAttribute( 'src', wp_get_attachment_image_url( $image_id, 'large' ) );
			$component->setAttribute( 'srcset', wp_get_attachment_image_srcset( $image_id, 'large' ) );
			$component->setAttribute( 'sizes', wp_get_attachment_image_sizes( $image_id, 'large' ) );
			$component->setAttribute( 'width', '210' );
			$component->setAttribute( 'height', '297' );
			$component->setAttribute( 'role', 'img' );
			$alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
			if ( ! empty( $alt ) ) {
				$component->setAttribute( 'alt', $alt );
			}
			$dom->appendChild( $component );
			return $dom->saveHTML();
		}
	}
}
